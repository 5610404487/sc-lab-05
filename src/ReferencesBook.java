public class ReferencesBook {
	private String namebook; 
	private int page;
	
	public ReferencesBook(String namebook, int page){
		this.namebook = namebook;
		this.page = page;
	}	
	
	public String getNameBook(){
		return this.namebook;
	}
	
	public void setNameBook(String namebook){
		this.namebook = namebook;
	}
}