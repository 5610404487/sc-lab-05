import Users.Users;

public class Main {
		public static void main(String[] args){
			Library lib = new Library();
			Users user = new Users(null, null, null, null);
			ReferencesBook ref = new ReferencesBook(null, 0);
			Users userAor = new Users("Undergrad","5610404193","Kallayaporn","Tisuthorn");
			Book bookAor = new Book("Big JAVA",1120,"17-02-15");
			System.out.println("User :"+user.toStringUsers("Undergrad","5610404193","Kallayaporn","Tisuthorn"));
			lib.borrowBook(userAor,bookAor);
			System.out.println("");
			Users userBoong = new Users("Undergrad","5610404487","Palida","Soonthonkiti");
			Book bookBoong = new Book("Big JAVA",1120,"17-02-15");
			System.out.println("User :"+user.toStringUsers("Undergrad","5610404487","Palida","Soonthonkiti"));
			lib.borrowBook(userBoong,bookBoong);
			System.out.println("");
			System.out.println("User :"+user.toStringUsers("Undergrad","5610404193","Kallayaporn","Tisuthorn"));
			lib.returnBook(userAor,bookAor);
		}
}
