package Users;


public class Users {
	Undergrad undergradUser = new Undergrad();
	Graduate graduateUser = new Graduate();
	PhDstudents phdUser = new PhDstudents();
	Instructor instructorUser = new Instructor();
	Officer officerUser = new Officer();
	
	public Users(String type,String id, String name,String last){
		if (type=="Undergrad"){
			undergradUser.Undergrad(id,name,last);
		}
		else if (type=="Graduate"){
			graduateUser.Graduate(id,name,last);
		}
		else if (type=="PhDstudents"){
			phdUser.PhDstudents(id,name,last);
		}
		else if (type=="Instructor"){
			instructorUser.Instructor(id,name,last);
		}
		else if (type=="Officer"){
			officerUser.Officer(id,name,last);
		}
	}
	public String toStringUsers(String type,String id,String name,String last){
		String a = "";
		if (type=="Undergrad"){
			undergradUser.Undergrad(id,name,last);
			a = undergradUser.toString();
		}
		else if (type=="Graduate"){
			graduateUser.Graduate(id,name,last);
			a = graduateUser.toString();
		}
		else if (type=="PhDstudents"){
			phdUser.PhDstudents(id,name,last);
			a = phdUser.toString();
		}
		else if (type=="Instructor"){
			instructorUser.Instructor(id,name,last);
			a = instructorUser.toString();
		}
		else if (type=="Officer"){
			officerUser.Officer(id,name,last);
			a = officerUser.toString();
		}
		return a;
		}
}

