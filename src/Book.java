
public class Book {
	private String namebook; 
	private int page;
	private String daymount;
	
	public Book(String namebook, int page,String daymount){
		this.namebook = namebook;
		this.page = page;
		this.daymount = daymount;
	}	
	
	public String getNameBook(){
		return this.namebook;
	}
	
	public void setNameBook(String namebook){
		this.namebook = namebook;
	}
}