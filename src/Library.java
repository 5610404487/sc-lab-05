import java.util.ArrayList;

import Users.Users;


public class Library {
	Users users = new Users(null, null, null, null);
	ReferencesBook refs = new ReferencesBook(null, 0);
	Book book = new Book(null, 0, null);
	ArrayList<String> borrowAlready = new ArrayList<String>();
	public void borrowBook(Users user,Book book){
		String status = "";
		String item = book.getNameBook();
		System.out.println("Name book: "+item);
		for(String b : borrowAlready){
			if(b.equals(item)){
				status = "false";
				System.out.println("You can't borrow this book.");
			}
			else{
				status = "true";
				System.out.println("You can borrow this book.");
				System.out.println("You borrow already.");
			}
		}
		AddBook(book,status);
		
	}
	public void returnBook(Users user,Book book){
		String status = "";
		String item = book.getNameBook();
		for(String b : borrowAlready){
			if(b==item){
				status = "true";
			}
			else{
				status = "false";
			}
		}
		 RemoveBook(book,status);
		 System.out.println("You return this book.");
	}
	public void AddBook(Book book,String status){
		String item = book.getNameBook();
		borrowAlready.add(item);
	}
	public void RemoveBook(Book book,String status){
		String item = book.getNameBook();
		borrowAlready.remove(item);
	}
}
